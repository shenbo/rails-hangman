require "spec_helper"

describe ApplicationHelper do
  describe "#full_title" do
    it "adds \"| Hangman\" to the end if page title provided" do
      expect(helper.full_title("Answers")).to eq "Answers | Hangman"
    end

    it "returns \"Hangman\" if no page title provided" do
      expect(helper.full_title("")).to eq "Hangman"
    end
  end
end
