require "spec_helper"

describe GamesHelper do
  describe "#marked_answer" do
    it "marks entire word if there is no space" do
      game = game_with_answer("WAND")
      expect(helper.marked_answer(game)).to eq "****"
    end

    it "marks letters but not spaces" do
      game = game_with_answer("HARRY POTTER")
      expect(helper.marked_answer(game)).to eq "***** ******"
    end
  end

  describe "#game_over_message" do
    let(:game) { game_with_answer("WAND") }

    it "returns \"WON!\" when won" do
      game.save
      correct_guesses = %w[W A N D]
      correct_guesses.each do |l|
        game.guessed_letters << GuessedLetter.new(letter: l, game_id: game.id)
      end
      expect(helper.game_over_message(game)).to eq "WON!"
    end

    it "returns \"LOST!\" with correct answer when lost" do
      game.save
      wrong_guesses = %w[B C E F G H I J K L]
      wrong_guesses.each do |l|
        game.guessed_letters << GuessedLetter.new(letter: l, game_id: game.id)
      end
      expect(helper.game_over_message(game)).to eq "LOST! The answer was #{game.word}"
    end

    it "returns nil when not won or lost" do
      expect(helper.game_over_message(game)).to be_nil
    end
  end

  def game_with_answer(word)
    answer = Answer.new(word: word)
    Game.new(answer: answer)
  end
end
