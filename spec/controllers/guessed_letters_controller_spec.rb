require 'rails_helper'

RSpec.describe GuessedLettersController, type: :controller do
  describe "POST create" do
    let(:word) { "HARRY POTTER" }
    let(:answer) { Answer.create!(word: word) }
    let(:game) { Game.create!(answer: answer) }

    it "creates a new guessed letter" do
      expect {
        post :create, params: { guessed_letter: { letter: "R", game_id: game.id } }
      }.to change(GuessedLetter, :count).by(1)
    end
  end
end
