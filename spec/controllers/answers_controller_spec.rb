require 'rails_helper'

RSpec.describe AnswersController, type: :controller do
  let(:word) { "HARRY POTTER" }
  let!(:answer) { Answer.create!(word: word) }

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "assigns all answers to @answers" do
      get :index
      expect(assigns(:answers)).to eq([answer])
    end

    it "assigns a new answer to @answer" do
      get :index
      expect(assigns(:answer)) == Answer.new
    end
  end

  describe "POST #create" do
    context "legit answer" do
      it "creates successfully" do
        expect {
          post :create, params: { answer: { word: "WAND" } }
        }.to change(Answer, :count).by(1)
      end

      it "redirects to answer index" do
        post :create, params: { answer: { word: "WAND" } }
        expect(response).to redirect_to(answers_url)
      end
    end

    context "not legit answer" do
      it "renders edit" do
        post :create, params: { answer: { word: "1" } }
        expect(response).to render_template(:edit)
      end
    end
  end

  describe "DELETE #destroy" do
    it "remove the answer" do
      expect {
        delete :destroy, params: { id: answer.id }
        }.to change(Answer, :count).by(-1)
    end

    it "redirects to answer index" do
      delete :destroy, params: { id: answer.id }
      expect(response).to redirect_to(answers_url)
    end
  end
end
