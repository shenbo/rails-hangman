require 'rails_helper'

RSpec.describe GamesController, type: :controller do
  let!(:word) { "HARRY POTTER" }
  let!(:answer) { Answer.create!(word: word) }

  describe "GET show" do
    let(:game) { Game.create!(answer: answer) }

    it "creates a GuessedLetter with the game id" do
      get :show, params: { id: game.id }
      expect(assigns(:guess_letter)).to be_a_new(GuessedLetter)
    end

    it "assigns the requested game to @game" do
      get :show, params: { id: game.id }
      expect(assigns(:game)).to eq(game)
    end

    it "renders the #show view" do
      get :show, params: { id: game.id }
      respond_to render_template :show
    end
  end

  describe "POST create" do
    let(:game) { Game.new(answer: answer) }

    it "creates a new game" do
      expect {
        post :create
      }.to change(Game, :count).by(1)
    end

    it "redirects to the game" do
      post :create
      expect(response).to redirect_to(game_path(Game.last.id))
    end
  end
end
