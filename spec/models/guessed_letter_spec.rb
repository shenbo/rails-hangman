require 'rails_helper'

RSpec.describe GuessedLetter, type: :model do
  let(:word) { "WAND" }
  let(:answer) { Answer.create!(word: word) }
  let(:game) { Game.create!(answer: answer) }

  it "only allows one uppercase letter, and a valid game" do
    expect(GuessedLetter.new(letter: "R", game_id: game.id)).to be_valid
  end

  it "letter cannot be empty" do
    expect(GuessedLetter.new(letter: " ", game_id: game.id)).to_not be_valid
    expect(GuessedLetter.new(letter: "", game_id: game.id)).to_not be_valid
  end

  it "letter cannot be a digit" do
    expect(GuessedLetter.new(letter: "1", game_id: game.id)).to_not be_valid
  end

  it "lowercase upcased" do
    expect(GuessedLetter.new(letter: "r", game_id: game.id)).to be_valid
  end

  it "letter cannot be more than one character" do
    expect(GuessedLetter.new(letter: "AA", game_id: game.id)).to_not be_valid
  end
end
