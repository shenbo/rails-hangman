require 'rails_helper'

RSpec.describe Answer, type: :model do
  describe "answer validation" do
    it "allows uppercase letters and spaces" do
      expect(Answer.new(word: "HARRY POTTER")).to be_valid
      expect(Answer.new(word: "WAND")).to be_valid
      expect(Answer.new(word: "ALBUS SEVERUS POTTER")).to be_valid
    end

    it "lowercase upcased" do
      expect(Answer.new(word: "Harry Potter")).to be_valid
    end

    it "must start and end with a letter" do
      expect(Answer.new(word: " WAND")).to_not be_valid
      expect(Answer.new(word: "WAND ")).to_not be_valid
    end

    it "cannot be empty" do
      expect(Answer.new(word: "")).to_not be_valid
      expect(Answer.new(word: " ")).to_not be_valid
    end

    it "does not allow funny characters" do
      expect(Answer.new(word: "HARRY POTTER=")).to_not be_valid
      expect(Answer.new(word: "HARRY\tPOTTER")).to_not be_valid
    end
  end
end
