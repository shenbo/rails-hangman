require 'rails_helper'

RSpec.describe Game, type: :model do
  let(:word) { "HARRY POTTER" }
  let(:answer) { Answer.create!(word: word) }
  let(:game) { Game.create!(answer: answer) }

  it "not valid without asnwer" do
    expect(Game.new).to_not be_valid
  end

  it "word correct" do
    expect(game.word).to eq word
  end

  it "new game has no guessed letters" do
    expect(game.all_guessed_letters).to be_empty
  end

  it "contains letter" do
    expect(game.contains_letter?('A')).to be true
    expect(game.contains_letter?('W')).to be false
  end

  it "new game has correct lives remain" do
    expect(game.lives_remain).to eq Game::NUM_OF_LIVES
  end

  it "new game not won or lost" do
    expect(game.won?).to be false
    expect(game.lost?).to be false
  end

  it "has correct guessed letters" do
    game.guessed_letters << GuessedLetter.create!(letter: 'A', game_id: game.id)
    expect(game.all_guessed_letters).to include('A')
    expect(game.all_guessed_letters).to_not include('W')

    expect(game.guessed_already?('A')).to be true
    expect(game.guessed_already?('Y')).to be false
  end

  it "can calculate game status correctly" do
    game.guessed_letters << GuessedLetter.create!(letter: 'H', game_id: game.id) # correct guess
    expect(game.lives_remain).to eq Game::NUM_OF_LIVES
    expect(game.won?).to be false
    expect(game.lost?).to be false

    game.guessed_letters << GuessedLetter.create!(letter: 'W', game_id: game.id) # wrong guess
    expect(game.lives_remain).to eq Game::NUM_OF_LIVES - 1

    correct_guesses = %w[A R Y P O T E]
    correct_guesses.each do |l|
      game.guessed_letters << GuessedLetter.create!(letter: l, game_id: game.id)
    end
    expect(game.won?).to be true
  end

  it "can lose the game" do
    game.guessed_letters << GuessedLetter.create!(letter: 'U', game_id: game.id) # wrong guess
    expect(game.lost?).to be false
    expect(game.lives_remain).to eq Game::NUM_OF_LIVES - 1

    wrong_guesses = %w[B C F M N V W X Z]
    wrong_guesses.each do |l|
      game.guessed_letters << GuessedLetter.create!(letter: l, game_id: game.id)
    end

    expect(game.lives_remain).to eq 0
    expect(game.lost?).to be true
  end
end
