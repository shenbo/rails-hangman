Given(/^new game page with answer "WAND"$/) do
  answer = Answer.create!(word: "WAND")
  game = Game.create!(answer: answer)
  visit game_path(game.id)
end

# Player win game
When(/^I guessed all letters$/) do
  correct_guesses = %w[W A N D]
  correct_guesses.each do |l|
    find('#guessed_letter_letter').set(l)
    click_button "Guess"
  end
end

Then(/^The game is finished$/) do
  expect(page).to have_no_content("Take a guess:")
end

Then(/^It tells me I won$/) do
  expect(page).to have_content("WON!")
end

Then(/^I can have a new game button$/) do
  expect(page).to have_button("Start New Game")
end

# Player lost game
When(/^I guessed all 10 letters and didn't win$/) do
  wrong_guesses = %w[B C E F G H I J K L]
  wrong_guesses.each do |l|
    find('#guessed_letter_letter').set(l)
    click_button "Guess"
  end
end

Then(/^It tells me I lost with the correct answer$/) do
  expect(page).to have_content("LOST! The answer was WAND")
end

# Player can see all guessed letters so far
When(/^I guessed A, B and C$/) do
  guesses = %w[A B C]
  guesses.each do |l|
    find('#guessed_letter_letter').set(l)
    click_button "Guess"
  end
end

Then(/^A, B and C are appended to Guessed Letters$/) do
  expect(page).to have_content("Guessed Letters: ABC")
end

# Player enter a letter that has been guessed, and is told so
When(/^I guessed A, B and A$/) do
  guesses = %w[A B A]
  guesses.each do |l|
    find('#guessed_letter_letter').set(l)
    click_button "Guess"
  end
end

Then(/^It warns me that the letter has been guessed already$/) do
  expect(page).to have_content("Letter has already been guessed")
end
