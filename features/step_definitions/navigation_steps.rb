When(/^I click on Home link$/) do
  click_on "Home"
end

Then(/^I'm on homepage$/) do
  expect(page).to have_current_path(root_path)
end

When(/^I click on Answers link$/) do
  click_on "Answers"
end

Then(/^I'm on answers page$/) do
  expect(page).to have_current_path(answers_path)
end
