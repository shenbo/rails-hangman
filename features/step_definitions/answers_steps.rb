When(/^I add (.*) as new answer$/) do |word|
  find('#answer_word').set(word)
  click_button "Add"
end

# Add new answer
Then(/^the page contains the new answer$/) do
  expect(page).to have_content("HARRY POTTER")
end

# Delete an answer
When(/^I delete an answer$/) do
  find('#answer_word').set("HARRY POTTER")
  click_button "Add"
  answer = Answer.find_by_word("HARRY POTTER")
  click_link answer.id
end

Then(/^the page does not contain the answer$/) do
  expect(page).to have_no_content("HARRY POTTER")
end

Then(/^it tells me this answer is invalid$/) do
  expect(page).to have_content("Word must be uppercase letters and spaces, must start and end with letters")
end
