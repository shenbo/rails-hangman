# Scenario: Player sees the welcome message
When(/^I go to the homepage$/) do
  visit root_path
end

Then(/^I should see the welcome message$/) do
  expect(page).to have_content("Welcome to the Hangman game!")
end

# Player can navigate to the rules page from homepage
When(/^I click on the rules link$/) do
  click_on "rules"
end

Then(/^I should be on the rules page$/) do
  expect(page).to have_current_path(rules_path)
end

# Player can navigate to the answers page from homepage
When(/^I click on the answers link$/) do
  click_on "here"
end

Then(/^I should be on the answers page$/) do
  expect(page).to have_current_path(answers_path)
end

# Player will be asked to create a new answer when try to start a game if no answers in db
When(/^No answers in the db$/) do
  Answer.delete_all
end

When(/^I click on the start new game button, no answers in the db$/) do
  click_button "Start New Game"
end

Then(/^I should be told to create a new answer$/) do
  expect(page).to have_content("Oops, we don't have any answers yet. How about create one here")
end

# Player can start a new game from homepage when there is at least one answer in db
When(/^There is at least one answer in db$/) do
  Answer.create!(word: "WAND")
end

When(/^I click on the start new game button$/) do
  click_button "Start New Game"
end

Then(/^I should be in a new game$/) do
  game = Game.last
  expect(page).to have_current_path(game_path(game))
  expect(page).to have_content("Lives Remaining: 10")
end
