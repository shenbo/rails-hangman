Given(/^I'm on the homepage$/) do
  visit root_path
end

Given(/^I'm on the answers page$/) do
  visit answers_path
end

Given(/^I'm on a game page$/) do
  answer = Answer.create!(word: "WAND")
  game = Game.create!(answer: answer)
  visit game_path(game.id)
end
