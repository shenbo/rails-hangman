Feature: Answers
As a game admin
I want a page to see all the answers, and add/remove answers
So it is easier for me to manage answers

Scenario: Add a new answer to the list of answers
Given I'm on the answers page
When I add HARRY POTTER as new answer
Then the page contains the new answer

Scenario: Delete an answer from the list of answers
Given I'm on the answers page
When I delete an answer
Then the page does not contain the answer

Scenario: Add new invalid answer with number, and is told it's invalid
Given I'm on the answers page
When I add 123 as new answer
Then it tells me this answer is invalid
