Feature: Play Game
As a game player
I want a working game
So that I can play it

Scenario: Player win game
Given new game page with answer "WAND"
When I guessed all letters
Then The game is finished
And It tells me I won
And I can have a new game button

Scenario: Player lost game
Given new game page with answer "WAND"
When I guessed all 10 letters and didn't win
Then The game is finished
And It tells me I lost with the correct answer
And I can have a new game button

Scenario: Player can see all guessed letters so far
Given new game page with answer "WAND"
When I guessed A, B and C
Then A, B and C are appended to Guessed Letters

Scenario: Player enter a letter that has been guessed, and is told so
Given new game page with answer "WAND"
When I guessed A, B and A
Then It warns me that the letter has been guessed already
