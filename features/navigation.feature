Feature: Navigation
As a game player
I want links to Home and Answers pages in all pages
So it is easier for me to navigate the app

Scenario: Navigate to homepage from answers page 
Given I'm on the answers page
When I click on Home link
Then I'm on homepage

Scenario: Navigate to homepage from game page 
Given I'm on a game page
When I click on Home link
Then I'm on homepage

Scenario: Navigate to answers page from homepage 
Given I'm on the homepage
When I click on Answers link
Then I'm on answers page
