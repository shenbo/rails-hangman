Feature: Homepage
As a game player
I want to see a home page that shows me a welcome message and some useful links
So that I have a better experience

Scenario: Player sees the welcome message
When I go to the homepage
Then I should see the welcome message

Scenario: Player can navigate to the rules page from homepage
Given I'm on the homepage
When I click on the rules link
Then I should be on the rules page

Scenario: Player can navigate to the answers page from homepage
Given I'm on the homepage
When I click on the answers link
Then I should be on the answers page

Scenario: Player will be asked to create a new answer when try to start a game if no answers in db
Given I'm on the homepage
And No answers in the db
When I click on the start new game button
Then I should be told to create a new answer

Scenario: Player can start a new game from homepage when there is at least one answer in db
Given I'm on the homepage
And There is at least one answer in db
When I click on the start new game button
Then I should be in a new game
