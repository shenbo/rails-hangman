Rails.application.routes.draw do
  get 'guessed_letters/new'
  root  'static_pages#welcome'
  get   '/rules', to: 'static_pages#rules'
  resources :answers
  resources :games
  resources :guessed_letters
end
