class CreateGuessedLetters < ActiveRecord::Migration[5.2]
  def change
    create_table :guessed_letters do |t|
      t.string :letter
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end
