class RemoveNbOfLivesFromGame < ActiveRecord::Migration[5.2]
  def change
    remove_column :games, :nb_of_lives, :number
  end
end
