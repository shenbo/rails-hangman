class AddNbOfLivesToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :nb_of_lives, :number
  end
end
