module GamesHelper
  UNREVEALED_LETTER = '*'.freeze

  def marked_answer(game)
    answer = game.word
    answer.chars.map do |c|
      game.guessed_already?(c) || c == ' ' ? c : UNREVEALED_LETTER
    end.join
  end

  def game_over_message(game)
    if game.won?
      "WON!"
    elsif game.lost?
      "LOST! The answer was #{game.word}"
    end
  end
end
