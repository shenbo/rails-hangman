class GamesController < ApplicationController
  def create
    if Answer.count == 0
      render "no_answers"
    else
      answer = Answer.order("RANDOM()").first
      @game = Game.new(answer: answer)
      @game.save
      redirect_to @game
    end
  end

  def show
    @guess_letter = GuessedLetter.new(game_id: params[:id])
    @game = Game.find(params[:id])
  end
end
