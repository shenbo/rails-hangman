class GuessedLettersController < ApplicationController
  def create
    @guess_letter = GuessedLetter.new(guess_letter_params)
    @game = Game.find(@guess_letter.game_id)

    if @guess_letter.save
      redirect_to @game
    else
      render "games/show"
    end
  end

  private

  def guess_letter_params
    params.require(:guessed_letter).permit(:letter, :game_id)
  end
end
