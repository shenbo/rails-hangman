class AnswersController < ApplicationController
  def index
    @answer = Answer.new
    @answers = Answer.all
  end

  def create
    @answer = Answer.new(answer_params)

    if @answer.save
      redirect_to answers_url
    else
      render "edit"
    end
  end

  def destroy
    Answer.find(params[:id]).destroy
    redirect_to answers_url
  end

  private

  def answer_params
    params.require(:answer).permit(:word)
  end
end
