class GuessedLetter < ApplicationRecord
  belongs_to :game
  validates :letter, uniqueness: { case_sensitive: false, 
                                   scope: :game_id,
                                   message: "has already been guessed" },
                     length:     { maximum: 1 }
  validates_format_of :letter, with: /\A[A-Z]\z/, message: "must be a uppercase letter"
  before_validation :upcase

  private

  def upcase
    letter.upcase!
  end
end
