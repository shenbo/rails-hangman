class Game < ApplicationRecord
  belongs_to :answer
  has_many :guessed_letters

  NUM_OF_LIVES = 10

  delegate :word, to: :answer

  def all_guessed_letters
    guessed_letters.pluck(:letter)
  end

  def contains_letter?(letter)
    word.include?(letter)
  end

  def guessed_already?(letter)
    all_guessed_letters.include?(letter)
  end

  def lives_remain
    NUM_OF_LIVES - (all_guessed_letters - word.chars).size
  end

  def won?
    (word.delete(' ').chars - all_guessed_letters).empty?
  end

  def lost?
    lives_remain <= 0
  end

  def display_all_guessed_letters
    all_guessed_letters.to_a.join
  end
end
