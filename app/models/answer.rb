class Answer < ApplicationRecord
  has_one :game
  validates :word, uniqueness: { message: "already exists" }
  validates_format_of :word, with: /\A[A-Z][A-Z ]*[A-Z]\z/,
                             message: "must be uppercase letters and spaces, must start and end with letters"
  before_validation :upcase

  private

  def upcase
    word.upcase!
  end
end
